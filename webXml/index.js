function loadXMLDoc() {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      myFunction(this);
    }
  };
  xmlhttp.open("GET", "datos.xml", true);
  xmlhttp.send();
}
function myFunction(xml) {
  var i;
  var xmlDoc = xml.responseXML;
  
  var x = xmlDoc.getElementsByTagName("datos");
  var salida = "";
  for (i = 0; i <x.length; i++) {
    salida += "<tr><td>" +
    x[i].getElementsByTagName("nombres")[0].childNodes[0].nodeValue +
    "</td><td>" +
    x[i].getElementsByTagName("apellidos")[0].childNodes[0].nodeValue +
    "</td><td>" +
    x[i].getElementsByTagName("semestre")[0].childNodes[0].nodeValue +
    "</td><td>" +
    x[i].getElementsByTagName("paralelo")[0].childNodes[0].nodeValue +
    "</td><td>" +
    x[i].getElementsByTagName("direccion")[0].childNodes[0].nodeValue +
    "</td><td>" +
    x[i].getElementsByTagName("correo")[0].childNodes[0].nodeValue +
    "</td></tr>";
  }
  document.getElementById("personas").innerHTML = salida;
}
loadXMLDoc()

